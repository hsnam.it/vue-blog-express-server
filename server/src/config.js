const dotenv = require("dotenv");
dotenv.config();

module.exports = {
  PORT: process.env.PORT,
  SECRET_KEY: process.env.SECRET_KEY,
  DATABASE_URL: "mongodb://localhost:27017",
  DATABASE_NAME: "dummyDb",
  SALT_ROUND: 10,
  JWT_EXPIRED_TIME: 1 // Hours
};
