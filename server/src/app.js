const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");
const mongoose = require("./db/mongoose");

const { PORT } = require("./config");

const app = express();
app.use(morgan("combined"));
app.use(bodyParser.json());
app.use(cors());

require("./routes")(app);

// Connect to database
mongoose.connect();

app.listen(PORT, () => {
  console.log(`The server is listening on http://localhost:${PORT}`);
});
