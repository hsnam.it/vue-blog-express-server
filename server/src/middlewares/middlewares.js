const { SECRET_KEY } = require("../config");
const jwt = require("jsonwebtoken");
const UserSession = require("../models/userSession");

const isAuthenticated = function(req, res, next) {
  if (typeof req.headers.authorization === "undefined") {
    return res.status(500).json({
      error: "Authorization information is required"
    });
  }

  let token = req.headers.authorization.split(" ")[1];
  jwt.verify(token, SECRET_KEY, { algorithm: "HS256" }, err => {
    if (err) {
      return res.status(401).json({ error: `Not authorized - ${err}` });
    }

    UserSession.findOne({ token: token })
      .then(session => {
        if (!session) {
          return res
            .status(401)
            .json({ error: "Not authorized: invalid token" });
        }

        if (session.is_logout) {
          return res.status(401).json({ error: "The user is logged out" });
        }

        req.headers.session = session;
        return next();
      })
      .catch(err => {
        res.status(500).send();
        return next(err);
      });
  });
};

module.exports = {
  isAuthenticated
};
