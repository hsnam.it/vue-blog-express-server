const ApiRouter = require("express").Router();

ApiRouter.use("/users", require("./users/users"));
// ApiRouter.use("/posts", require("./posts"));

ApiRouter.get("/", (req, res) => {
  return res.send({
    msg: "You know, for blog backend"
  });
});

module.exports = ApiRouter;
