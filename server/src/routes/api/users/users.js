const express = require("express");
const jwt = require("jsonwebtoken");

const User = require("../../../models/user");
const UserSession = require("../../../models/userSession");

const { isAuthenticated } = require("../../../middlewares/middlewares");
const { JWT_EXPIRED_TIME, SECRET_KEY } = require("../../../config");

const UserRouter = express.Router();

UserRouter.post("/register", (req, res) => {
  if (
    typeof req.body.email === "undefined" ||
    typeof req.body.name === "undefined" ||
    typeof req.body.password === "undefined"
  ) {
    return res.status(400).send({
      error: "Name, email and password are required to register new user"
    });
  }

  User.create({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    avatar: req.body.avatar
  })
    .then(newUser => {
      res.send({
        msg: `Hello ${newUser.name}, your user was registered!`
      });
    })
    .catch(err => {
      res.status(400).send({
        error: err.errmsg
      });
    });
});

UserRouter.post("/login", (req, res) => {
  if (
    typeof req.body.email === "undefined" ||
    typeof req.body.password === "undefined"
  ) {
    return res.status(400).send({
      error: "Email and password are required to login"
    });
  }

  // Get user info
  let email = req.body.email;
  let password = req.body.password;

  User.findOne({ email: email }, (err, user) => {
    if (err) {
      console.log(err);
      return res.status(500).send();
    }

    if (!user)
      return res.status(401).send({
        error: "Couldn't login, please check your email and password"
      });

    // Compare password
    user
      .comparePassword(password)
      .then(isMatch => {
        if (!isMatch)
          return res.status(401).send({
            error: "Couldn't login, your password was wrong"
          });

        let payload = {
          email: user.email,
          name: user.name,
          exp: Math.floor(Date.now() / 1000) + JWT_EXPIRED_TIME * 60 * 60
        };

        // Create jwt token
        const token = jwt.sign(payload, SECRET_KEY, { algorithm: "HS256" });
        // Add to session table
        UserSession.create({ token: token });

        return res.send({ email: email, token: token });
      })
      .catch(err => {
        console.log(err);
        return res.status(500).send();
      });
  });
});

UserRouter.post("/logout", isAuthenticated, (req, res) => {
  req.headers.session.logout();
  return res.send({ msg: "Logged out successfully" });
});

module.exports = UserRouter;
