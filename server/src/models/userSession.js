const mongoose = require("mongoose");

let UserSessionSchema = new mongoose.Schema({
  token: {
    type: String,
    required: true,
    unique: true
  },
  is_logout: {
    type: Boolean,
    default: false
  }
});

UserSessionSchema.methods.logout = function() {
  let userSession = this;
  userSession.is_logout = true;
  userSession.save();
};

module.exports = mongoose.model("UserSession", UserSessionSchema);
