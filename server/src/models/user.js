const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const { SALT_ROUND } = require("../config");

let User = new mongoose.Schema(
  {
    name: {
      required: true,
      type: String
    },
    email: {
      required: true,
      type: String,
      unique: true
    },
    password: {
      required: true,
      type: String
    },
    avatar: String,
    is_super: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: true }
);

// Hook for pre-save event
User.pre("save", function(next) {
  let user = this;

  bcrypt
    .hash(user.password, SALT_ROUND)
    .then(hashedPassword => {
      user.password = hashedPassword;
      next();
    })
    .catch(err => {
      next(err);
      throw new Error(err);
    });
});

User.methods.comparePassword = function(candidatePassword) {
  return new Promise((resovle, reject) => {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
      if (err) {
        reject(err);
      }
      resovle(isMatch);
    });
  });
};

module.exports = mongoose.model("user", User);
