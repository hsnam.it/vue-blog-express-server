const { DATABASE_URL, DATABASE_NAME } = require("../config.js");
const mongoose = require("mongoose");

module.exports = {
  connect() {
    try {
      mongoose.Promise = global.Promise;
      mongoose.connect(`${DATABASE_URL}/${DATABASE_NAME}`, {
        useNewUrlParser: true,
        useCreateIndex: true
      });
    } catch (err) {
      throw new Error(err);
    }
  }
};
