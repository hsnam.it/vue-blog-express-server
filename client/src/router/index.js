import Vue from "vue";
import Router from "vue-router";

import BlogSection from "../pages/BlogSection.vue";
import AdminSection from "../pages/AdminSection.vue";

import AdminHomePage from "../components/admin/AdminHomePage.vue";
import Posts from "../components/admin/Posts.vue";
import NewPost from "../components/admin/NewPost.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      component: BlogSection
    },
    {
      path: "/",
      component: AdminSection,
      children: [
        {
          path: "admin",
          component: AdminHomePage,
          children: [
            {
              path: "all-posts",
              component: Posts
            },
            {
              path: "new-post",
              component: NewPost
            }
          ]
        }
      ]
    }
  ]
});
