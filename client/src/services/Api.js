import axios from "axios";

export default () => {
  return axios.create({
    baseURL: "http://localhost:8081" //TODO: Need to save this value in a separate file
  });
};
