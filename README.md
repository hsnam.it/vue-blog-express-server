## Current video:

https://www.youtube.com/watch?v=Fa4cRMaTDUI

## Current tutorial:

https://medium.com/swlh/a-practical-guide-for-jwt-authentication-using-nodejs-and-express-d48369e7e6d4

## Things TODO:

- Setup morgan and cors
- Install SSL for client and server

## How to re-structure express server using router

- http://billpatrianakos.me/blog/2015/12/01/organizing-express-routes/
- https://expressjs.com/en/guide/routing.html
